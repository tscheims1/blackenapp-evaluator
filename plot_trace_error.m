clc;
%import export4x4_medium_real.csv

%refPointLatLngAlt
wgs84Ref = [LatMission(1,1),LngMission(1,1),0];
ecefRef = [MissionX0(1,1),MissionY0(1,1),MissionZ0(1,1)];


lastWaypoint = 40;
lineWith = 1;

XYMission = [MissionX0,MissionY0,MissionZ0];
XYDrone = [DroneX0,DroneY0,DroneZ0];
XYMissionLocal = []
for i=1:min(lastWaypoint,size(XYMission))
    XYMissionLocal(i,:) =ToENUCoordinate(XYMission(i,:),wgs84Ref,ecefRef);
end
XYDroneLocal  = []
for i=1:min(lastWaypoint,size(XYDrone))
    XYDroneLocal(i,:) =ToENUCoordinate(XYDrone(i,:),wgs84Ref,ecefRef);

    
end
plot(XYMissionLocal(:,1),XYMissionLocal(:,2),'b*');
hold on
plot(XYDroneLocal(:,1),XYDroneLocal(:,2),'r*');
plot(XYMissionLocal(:,1),XYMissionLocal(:,2),'b');
plot(XYDroneLocal(:,1),XYDroneLocal(:,2),'r');


XYMissionLocal(:,3) = 0
XYDroneLocal(:,3) = 0
XYDistance = XYMissionLocal-XYDroneLocal
sumedNorm = 0
for i=1:size(XYDistance)
 sumedNorm = sumedNorm +norm(XYDistance(i,:))   
end
sumedNorm./size(XYDistance,1)



h_legend = legend('Wegpunkte geplant','Wegpunkte geflogen','Location','northwest');
set(h_legend,'FontSize',30);
xlbl = xlabel('Osten [m]');
set(xlbl,'FontSize',20);
ylbl = ylabel('Norden [m]');
set(gca,'FontSize',20)
set(ylbl,'FontSize',20);
axis equal
grid on


hold off
