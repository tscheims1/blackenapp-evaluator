import numpy as np
import math
class GpsCoordinate:
	
	EARTH_RADIUS = 6371000*1.0
	
	def __init__(self,lat,lng,alt):
		self.lat = float(lat)
		self.lng = float(lng)
		self.alt = float(alt)
		
	
	def toCartesianCoordinate(self):

		lat = math.radians(self.lat)
		lng = math.radians(self.lng)
		
		
		x = (GpsCoordinate.EARTH_RADIUS+self.alt)*math.cos(lat)*math.cos(lng)
		y = (GpsCoordinate.EARTH_RADIUS+self.alt)*math.cos(lat)*math.sin(lng)
		z = (GpsCoordinate.EARTH_RADIUS+self.alt)*math.sin(lat)
		
		return np.array([x,y,z])
		
