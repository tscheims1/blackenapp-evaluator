"""
	this read the WGS84 positions from the dji phantom 3 professional photos
"""

from GeoPositionEvaluator import GeoPositionEvaluator
from GpsWaypointImporter import GpsWaypointImporter
from GeoPositionExifReader import GeoPositionExifReader


class BlackenappEvaluator:
	
	def __init__(self,imageFolder,waypointFile):
		self.imageFolder = imageFolder
		self.wayPointFile = waypointFile
		self.geoPositionEvaluator = None
	def evaluate(self):
				
		gpsWaypointImporter = GpsWaypointImporter(self.wayPointFile)
		gpsWaypointImporter.importFile()
		waypoints = gpsWaypointImporter.getGpsData()
		
		

		geoPositionExifReader = GeoPositionExifReader(self.imageFolder)
		geoPositionExifReader.readExifData()

		self.geoPositionEvaluator = GeoPositionEvaluator(gpsWaypointImporter.getGpsData(),
												geoPositionExifReader.getGpsData())
												
		self.geoPositionEvaluator.evaluate()
			
	def printToFile(self,fileName):
		return self.geoPositionEvaluator.printToFile(fileName)	


blackenAppEvaluator = BlackenappEvaluator('trace10x10_simulator','waypointexport_10x10_simulator.csv')
blackenAppEvaluator.evaluate()
blackenAppEvaluator.printToFile('export10x10_simulator.csv')


blackenAppEvaluator = BlackenappEvaluator('trace10x10_medium_real','waypointexport_10x10_real.csv')
blackenAppEvaluator.evaluate()
blackenAppEvaluator.printToFile('export10x10_medium_real.csv')

		

blackenAppEvaluator = BlackenappEvaluator('trace10x10_slow_real','waypointexport_10x10_real.csv')
blackenAppEvaluator.evaluate()
blackenAppEvaluator.printToFile('export10x10_slow_real.csv')


blackenAppEvaluator = BlackenappEvaluator('trace4x4_medium_real','waypointexport_4x4_real.csv')
blackenAppEvaluator.evaluate()
blackenAppEvaluator.printToFile('export4x4_medium_real.csv')



blackenAppEvaluator = BlackenappEvaluator('trace4x4_slow_real','waypointexport_4x4_real.csv')
blackenAppEvaluator.evaluate()
blackenAppEvaluator.printToFile('export4x4_slow_real.csv')
