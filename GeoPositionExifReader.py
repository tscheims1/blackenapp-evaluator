import PIL.Image
from PIL.ExifTags import TAGS, GPSTAGS
import os
from GpsCoordinate import  GpsCoordinate
class GeoPositionExifReader:
	
	
	def __init__(self,folder):
		self.folder = folder
		self.gpsDataStore = {}
	
	def readExifData(self):
		
		 files = os.listdir(self.folder)
		 for file in files:
			 img = PIL.Image.open(os.path.join(self.folder,file))
			 exifData = img._getexif()
			 
			 for tag,value in exifData.items():
				decoded = TAGS.get(tag, tag)
				if decoded == "GPSInfo":
					gpsData = {}
					for t in value:
						subDecoded = GPSTAGS.get(t, t)
						gpsData[subDecoded] = value[t]
					
					lat,lng,alt = self.readGpsCoordinatesFromExif(gpsData)
					self.gpsDataStore[file] = GpsCoordinate(lat,lng,alt)
					
					
	def convertExifGpsToDegree(self,value):
		d0 = value[0][0]
		d1 = value[0][1]
		d = float(d0) / float(d1)

		m0 = value[1][0]
		m1 = value[1][1]
		m = float(m0) / float(m1)

		s0 = value[2][0]
		s1 = value[2][1]
		s = float(s0) / float(s1)

		return d + (m / 60.0) + (s / 3600.0)
		 
		 
	def readGpsCoordinatesFromExif(self,gpsInfo):
		
		lat  = gpsInfo["GPSLatitude"]
		latRef = gpsInfo["GPSLatitudeRef"]
		lng = gpsInfo["GPSLongitude"]
		lngRef = gpsInfo["GPSLongitudeRef"]
		alt = gpsInfo["GPSAltitude"]
		
		lat = self.convertExifGpsToDegree(lat)
		lng = self.convertExifGpsToDegree(lng)
		alt = alt[0]/(alt[1]*1.0)
		
		if(latRef != "N"):
			lat = 0 - lat
		if(lngRef != "E"):
			lng = 0- lng
			
		return lat,lng,alt
	
	def getGpsData(self):
		return self.gpsDataStore
		
		

