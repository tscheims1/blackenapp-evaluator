# Blackenapp-Evaluator

Just a few python scripts for evaluating the accuracy of the DJI Phantom 3 Professional flights.


## usage

1. run the following command: ```python2 BlackenappEvaluator.py```
2. the script generates several csv export files
3. Import these files in the ```plot_trace_error.m```-Script for visualizing the driven route.