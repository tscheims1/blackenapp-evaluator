from GpsCoordinate import GpsCoordinate
from GpsEvaluationContainer import GpsEvaluationContainer
import numpy as np
import sys
import copy
class GeoPositionEvaluator:
	
	def __init__(self,gpsDataMission,gpsDataDrone):
		self.gpsDataDrone = gpsDataDrone
		self.gpsDataMission = gpsDataMission
		self.evaluationContainers = []
		
	"""
		search nearest neighbor and store the total delta
	"""
	def evaluate(self):
		
		gpsDataDroneCartesian = {}
		gpsDataMissionCartesian = {}
		gpsDataMissionCartesianXY = {}
		gpsDataDroneCartesianXY = {}
		
		
		for key,value in self.gpsDataDrone.items():
			print(value.lat,value.lng,value.alt)
			gpsDataDroneCartesian[key] = value.toCartesianCoordinate()
			value2 = copy.deepcopy(value)
			value2.alt =0
			gpsDataDroneCartesianXY[key] = value2.toCartesianCoordinate()  
		
		for key,value in self.gpsDataMission.items():
			print(value.lat,value.lng,value.alt)
			gpsDataMissionCartesian[key] = value.toCartesianCoordinate()
			value2 = copy.deepcopy(value)
			value2.alt = 0
			gpsDataMissionCartesianXY[key] = value2.toCartesianCoordinate()
			
		totalDistance = 0
		for key,value in gpsDataMissionCartesian.items():
			
			minElement = None
			minDistance = float('inf')
			for key2,value2 in gpsDataDroneCartesian.items():
				distanceVector = value-value2
				
				
				distance = np.linalg.norm(distanceVector)
				
				if(minDistance > distance):
					minDistance = distance
					minElement = key2
					minDistanceXY = np.linalg.norm(gpsDataDroneCartesianXY[key2] - gpsDataMissionCartesianXY[key])
					
			
			totalDistance += minDistance
			print(minDistance)
			print(minDistanceXY)
			self.evaluationContainers.append(GpsEvaluationContainer(minElement,
					self.gpsDataMission[key],self.gpsDataDrone[minElement],
					gpsDataMissionCartesian[key],gpsDataDroneCartesian[minElement],gpsDataMissionCartesianXY[key],
					gpsDataDroneCartesianXY[minElement],minDistance,minDistanceXY))
													  
			
			
		print(totalDistance)
	def printToFile(self,fileName):
		fileHandle = open(fileName,'w')
		
		i = 0
		for line in self.evaluationContainers:
			if(i == 0):
				fileHandle.write(line.printHeader())
			i+=1
			fileHandle.write(line.toCSVLine())
			
		fileHandle.close()
	
		
