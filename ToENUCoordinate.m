function xyzPos = ToENUCoordinate(ecef,wgs84Ref,ecefRef)
    lambda2 = degtorad(wgs84Ref(1,1))
    phi2 = degtorad(wgs84Ref(1,2))
    
    transformMat = [-sin(lambda2),cos(lambda2),0;
                    -sin(phi2)*cos(lambda2),-sin(phi2)*sin(lambda2),cos(phi2);
                    cos(phi2)*cos(lambda2),cos(phi2)*sin(lambda2),sin(phi2)];
    
    deltaVector = ecefRef-ecef;
    
    result  = transformMat *deltaVector';
    xyzPos = result';
                
end