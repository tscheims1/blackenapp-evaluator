from GpsCoordinate import GpsCoordinate


class GpsEvaluationContainer:
	
	def __init__(self,name,coordMission,coordDrone,xyzMission,xyzDrone,xyMission,xyDrone,minDistance,minDistanceXY):
		self.name = name
		self.coordMission  = coordMission
		self.coordDrone = coordDrone
		self.xyzMission = xyzMission
		self.xyzDrone = xyzDrone
		self.minDistance = minDistance
		self.minDistanceXY = minDistanceXY
		self.xyMission = xyMission
		self.xyDrone = xyDrone
		
	def printHeader(self):
		return "Filename;LatMission;LngMission;AltMission;MissionX;MissionY;MissionZ;MissionX0;MissionY0;MissionZ0;LatDrone;LngDrone;AltDrone;DroneX;DroneY;DroneZ;DroneX0;DroneY0;DroneZ0;MinDistance;MinDistanceXY\n"
		
	def toCSVLine(self):
		 		
			
		coordMissionString = str(self.coordMission.lat) +";"+str(self.coordMission.lng) +";"+str(self.coordMission.alt)
		xyzMissionString = str(self.xyzMission[0])+";"+str(self.xyzMission[1])+";"+str(self.xyzMission[2])
		coorDroneString = str(self.coordDrone.lat) +";"+str(self.coordDrone.lng)+";"+str(self.coordDrone.alt)
		xyzDroneString = str(self.xyzDrone[0])+";"+str(self.xyzDrone[1])+";"+str(self.xyzDrone[2])
		xyMissionString = str(self.xyMission[0])+";"+str(self.xyMission[1])+";"+str(self.xyMission[2])
		xyDroneString = str(self.xyDrone[0])+";"+str(self.xyDrone[1])+";"+str(self.xyDrone[2])
		
		return self.name+";"+coordMissionString+";"+xyzMissionString+";"+xyMissionString+";"+coorDroneString+";"+xyzDroneString+";"+xyDroneString+";"+str(self.minDistance)+";"+str(self.minDistanceXY)+"\n"
